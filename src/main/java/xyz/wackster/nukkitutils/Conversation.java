package xyz.wackster.nukkitutils;

import cn.nukkit.command.PluginIdentifiableCommand;

public class Conversation {
    public void begin() {
    }

    public void outputNextPrompt() {
    }

    public void abandon(ConversationAbandonedEvent details) {
    }

    public PluginIdentifiableCommand getContext() {
    }
}
